  _____                _   __  __      
 |  __ \              | | |  \/  |     
 | |__) |___  __ _  __| | | \  / | ___ 
 |  _  // _ \/ _` |/ _` | | |\/| |/ _ \
 | | \ \  __/ (_| | (_| | | |  | |  __/
 |_|  \_\___|\__,_|\__,_| |_|  |_|\___|
        
       Spring Boot application
       Author: Nemanja Djokic 
        

       
REST endpoints with different HTTP methods. 

1.  HTTP GET - list all superheroes
	URL: https://localhost:8080/api/superhero/
   
2.  HTTP GET - list single superhero
	URL: https://localhost:8080/api/superhero/{id}

3.  HTTP POST - create new superhero
	URL: https://localhost:8080/api/superhero/
	
4.  HTTP PUT - update superhero
	URL: https://localhost:8080/api/superhero/{id}
	
5.  HTTP DELETE - delete single superhero
	URL: https://localhost:8080/api/superhero/{id}
	
	
Those endpoints can be tested using REST client such as Postman. 

Another way to test is to use Swagger framework. Its used for documenting, describing and consuming  
REST Web services. Swagger interact directly with API through Swagger UI which can be found in URL below:

https://localhost:8080/swagger-ui.html