package com.spring.rest;

import java.text.ParseException;

import org.apache.http.HttpStatus;
import org.hamcrest.CoreMatchers;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.authentication.PreemptiveBasicAuthScheme;
import com.jayway.restassured.http.ContentType;
import com.spring.rest.model.Superhero;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SuperheroRestIT {

	@LocalServerPort
	private int serverPort;	
	
	@Before
	public void setUp() {
		RestAssured.port = serverPort;
			
		PreemptiveBasicAuthScheme authScheme = new PreemptiveBasicAuthScheme();
		authScheme.setUserName("testuser");
		authScheme.setPassword("pass123");
		RestAssured.authentication = authScheme;
	}
	
	@Test
	public void it1_1_listSuperheroWhichDoesNotExists() {
		RestAssured.when()
		.get("api/superhero/7")
		.then()
		.statusCode(HttpStatus.SC_NOT_FOUND);
	}
	
	@Test
	public void it1_2_listSuperhero() {
		RestAssured.when()
		.get("api/superhero/2")
		.then()
		.statusCode(HttpStatus.SC_OK)
		.contentType(ContentType.JSON)
		.body("name", CoreMatchers.notNullValue())
		.body("name", CoreMatchers.is("Robin"));
	}
	
	@Test
	public void it2_listAllSuperheroes() {
		RestAssured.when()
		.get("api/superhero/")
		.then()
		.statusCode(HttpStatus.SC_OK)
		.contentType(ContentType.JSON)
		.body("name", CoreMatchers.hasItems("Batman", "Robin", "Wonderwomen", "Superman"));
	}
	
	@Test 
	public void it3_1_createSuperheroWhichExists() throws ParseException {
		Superhero superhero = new Superhero(1l, "Batman");
		
		RestAssured.given()
		.body(superhero)
		.contentType(ContentType.JSON)
		.when()
		.post("api/superhero/")
		.then()
		.statusCode(HttpStatus.SC_CONFLICT);	
	}
	
	@Test 
	public void it3_2_createSuperhero() throws ParseException {
		Superhero superhero = new Superhero(0, "Loki");
		
		RestAssured.given()
		.body(superhero)
		.contentType(ContentType.JSON)
		.when()
		.post("api/superhero/")
		.then()
		.statusCode(HttpStatus.SC_CREATED);	
	}
	
	@Test
	public void it4_1_updateNonExistingSuperhero() {
		Superhero superhero = new Superhero();
		
		RestAssured.given()
		.body(superhero)
		.contentType(ContentType.JSON)
		.when()
		.put("api/superhero/6")
		.then()
		.statusCode(HttpStatus.SC_NOT_FOUND);
	}
	
	@Test
	public void it4_2_updateSuperhero() throws ParseException {
		Superhero superhero = new Superhero();
		superhero.setName("Updated name");
		
		RestAssured.given()
		.body(superhero)
		.contentType(ContentType.JSON)
		.when()
		.put("api/superhero/1")
		.then()
		.statusCode(HttpStatus.SC_OK)
		.body("name", CoreMatchers.is("Updated name"));	
	}
	
	@Test
	public void it5_1_deleteSuperheroWhichDoesNotExists() {
		RestAssured
		.when()
		.delete("api/superhero/7")
		.then()
		.statusCode(HttpStatus.SC_NOT_FOUND);
	}
	
	@Test
	public void it5_2_deleteSuperhero() {
		RestAssured
		.when()
		.delete("api/superhero/1")
		.then()
		.statusCode(HttpStatus.SC_OK);
	}
	
}
