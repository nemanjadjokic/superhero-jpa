//package com.spring.rest;
//
//import java.text.ParseException;
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.List;
//
//import org.junit.Assert;
//import org.junit.FixMethodOrder;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.junit.runners.MethodSorters;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//import org.springframework.test.context.web.WebAppConfiguration;
//
//import com.spring.rest.model.Superhero;
//import com.spring.rest.service.SuperheroService;
//
//@WebAppConfiguration
//@RunWith(SpringJUnit4ClassRunner.class)
//@SpringBootTest(classes = SuperheroApplication.class)
//@FixMethodOrder(MethodSorters.NAME_ASCENDING)
//public class SuperheroServiceTest {
//
//	@Autowired
//	SuperheroService superheroService;
//	
//	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//	
//	@Test
//	public void ut1_testGetSuperhero() throws ParseException {
//		Superhero superheroOne = superheroService.getSuperheroById(1l);
//		Superhero superheroTwo = new Superhero(1l,  "Robin", "Robin", "DC Comics", 
//				new ArrayList<String>(Arrays.asList("Fighter", "Acrobat", "Escapologist")), 
//				null, sdf.parse("1939-03-01"));
//		
//		Assert.assertEquals(superheroOne.getId(), superheroTwo.getId());
//		Assert.assertEquals(superheroOne.getName(), superheroTwo.getName());
//		Assert.assertEquals(superheroOne.getPseudonym(), superheroTwo.getPseudonym());
//		Assert.assertEquals(superheroOne.getPublisher(), superheroTwo.getPublisher());
//		Assert.assertEquals(superheroOne.getSkills(), superheroTwo.getSkills());
//		Assert.assertEquals(superheroOne.getAllies(), superheroTwo.getAllies());
//	}
//	
//	@Test
//	public void ut2_testGetAllSuperheroes() {
//		List<Superhero> superheroes = superheroService.getAllSuperheroes();
//		Assert.assertNotEquals(null, superheroes);
//		Assert.assertNotNull(superheroes);
//	}
//	
//	@Test
//	public void ut3_saveSuperhero() throws ParseException {
//		int counter = superheroService.getAllSuperheroes().size();
//		
//		Superhero superhero = new Superhero(0, "Loki", "Loki Laufeyson", "Marvel Comics", 
//				new ArrayList<String>(Arrays.asList("Master manipulator", "Astral projection", "Tactician and strategist", "Illusion casting")), 
//				null, sdf.parse("1962-10-01"));
//		
//		superheroService.saveSuperhero(superhero);	
//		
//		int new_counter = superheroService.getAllSuperheroes().size(); 
//		
//		Assert.assertEquals(counter + 1, new_counter);
//		Assert.assertEquals(superheroService.getSuperheroById(new_counter).getId(), superhero.getId());
//		Assert.assertEquals(superheroService.getSuperheroById(new_counter).getPseudonym(), superhero.getPseudonym());
//		Assert.assertEquals(superheroService.getSuperheroById(new_counter).getName(), superhero.getName());
//	}
//	
//	@Test
//	public void ut4_testUpdateSuperhero() {
//		Superhero superhero = superheroService.getSuperheroById(2l);
//		superhero.setName("Updated name");
//		superhero.setPublisher("Updated publisher");
//		superheroService.updateSuperhero(superhero);
//		Assert.assertEquals(superhero.getName(), superheroService.getSuperheroById(2l).getName());
//		Assert.assertEquals(superhero.getPublisher(), superheroService.getSuperheroById(2l).getPublisher());	
//	}
//	
//	@Test
//	public void ut5_testDeleteSuperhero() {
//		superheroService.deleteSuperheroById(2l);
//		Superhero superhero = superheroService.getSuperheroById(2l);
//		Assert.assertNull(superhero);
//	}
//	
//	@Test
//	public void ut6_testDeleteAllSuperheroes() {
//		superheroService.deleteAllSuperheroes();
//		List<Superhero> superheros = superheroService.getAllSuperheroes();
//		Assert.assertEquals(superheros.size(), 0);
//	}
//	
//
//
//}
