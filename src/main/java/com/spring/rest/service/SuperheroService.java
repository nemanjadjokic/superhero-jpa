package com.spring.rest.service;

import java.util.List;

import com.spring.rest.model.Superhero;

public interface SuperheroService {
	
	Superhero getSuperheroById(long id);
	
	List<Superhero> getAllSuperheroes();
	
	void saveSuperhero(Superhero superhero);
	
	void deleteSuperheroById(long id);
	
	boolean doSuperheroExists(Superhero superhero);
	
}
