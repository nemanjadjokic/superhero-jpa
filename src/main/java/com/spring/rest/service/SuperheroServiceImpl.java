package com.spring.rest.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spring.rest.model.Superhero;
import com.spring.rest.repository.SuperheroRepository;

@Service
@Transactional
public class SuperheroServiceImpl implements SuperheroService {

	@Autowired
	private SuperheroRepository superheroRepository;
	
	public Superhero getSuperheroById(long id) {
		Superhero superhero = superheroRepository.findOne(id);
		if (superhero != null) {
			return superhero;
		} else {
			return null;
		}
	}

	public List<Superhero> getAllSuperheroes() {
		return superheroRepository.findAll();
	}

	public void saveSuperhero(Superhero superhero) {
		superheroRepository.save(superhero);
	}
	
	public void deleteSuperheroById(long id) {
		superheroRepository.delete(id);
	}

	public boolean doSuperheroExists(Superhero superhero) {
		return getSuperheroByName(superhero.getName()) != null;
	}
	
	private Superhero getSuperheroByName(String name) {
		for (Superhero superhero : superheroRepository.findAll()) {
			if (superhero.getName().equalsIgnoreCase(name)) {
				return superhero;
			}
		}
		return null;
	}
	
}
