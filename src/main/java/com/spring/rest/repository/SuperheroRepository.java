package com.spring.rest.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.spring.rest.model.Superhero;

@Repository
public interface SuperheroRepository  extends JpaRepository<Superhero, Long>{

}
