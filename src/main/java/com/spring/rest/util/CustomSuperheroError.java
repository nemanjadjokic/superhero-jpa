package com.spring.rest.util;

public class CustomSuperheroError {

	private String errorMessage;

	public CustomSuperheroError(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getErrorMessage() {
		return errorMessage;
	}
	
}
