package com.spring.rest.controller;

import com.spring.rest.model.Superhero;
import com.spring.rest.service.SuperheroService;
import com.spring.rest.util.CustomSuperheroError;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

@Api(value="Superheroes REST Controller")
@RestController
@RequestMapping(value = "api/superhero")
public class SuperheroRESTController {

	@Autowired
	SuperheroService superheroService;

	@ApiOperation(value = "List single superhero by ID", notes = "REST call which return single superhero")
	@GetMapping("{id}")
	public ResponseEntity<?> listSuperhero(@PathVariable("id") long id) {
		Superhero superhero = superheroService.getSuperheroById(id);
		if (superhero == null) {
			return new ResponseEntity<>(new CustomSuperheroError("Superhero with ID: " + id + " not found."), HttpStatus.NOT_FOUND);
		} 
		return new ResponseEntity<Superhero>(superhero, HttpStatus.OK);
	}
	
	@ApiOperation(value = "List all superheroes", notes = "REST call which returns all superheroes")
	@GetMapping
	public ResponseEntity<?> listAllSuperheroes() {
		List<Superhero> superheroes = superheroService.getAllSuperheroes();
		if (superheroes.isEmpty()) {
			return new ResponseEntity<>(new CustomSuperheroError("No content to display."), HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<List<Superhero>>(superheroes, HttpStatus.OK);		
	}
	
	@ApiOperation(value = "Create new superhero", notes = "REST call which create new superhero")
	@PostMapping
	public ResponseEntity<?> createSuperhero(@RequestBody Superhero superhero, UriComponentsBuilder ucb) {
		if (superheroService.doSuperheroExists(superhero)) {
			return new ResponseEntity<>(new CustomSuperheroError("Superhero with name: " + 
						superhero.getName() + " allready exists."), HttpStatus.CONFLICT);
		}
		superheroService.saveSuperhero(superhero);
		
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(ucb.path("/superhero/{id}").buildAndExpand(superhero.getId()).toUri());
		
		return new ResponseEntity<String>(headers, HttpStatus.CREATED);
	}
	
	@ApiOperation(value = "Update superhero", notes = "REST call which update superhero")
	@PutMapping("{id}")
	public ResponseEntity<?> updateSuperhero(@PathVariable long id, @RequestBody Superhero superhero) {
				
		Superhero temp_superhero = superheroService.getSuperheroById(id);
		
		if (temp_superhero == null) {
			return new ResponseEntity<>(new CustomSuperheroError("Unable to update, user with ID: " + id + " not found."), HttpStatus.NOT_FOUND);
		}
		
		if (superhero.getName() != null) {
			temp_superhero.setName(superhero.getName());
		} 
		
		superheroService.saveSuperhero(temp_superhero);
		return new ResponseEntity<Superhero>(temp_superhero, HttpStatus.OK);
	}
	
	@ApiOperation(value = "Delete single superhero", notes = "REST call which delete superhero")
	@DeleteMapping("{id}")
	public ResponseEntity<?> deleteSuperhero(@PathVariable("id") long id) {
		Superhero superhero = superheroService.getSuperheroById(id);
		if (superhero == null) {
			return new ResponseEntity<>(new CustomSuperheroError("Unable to delete, superhero with ID: " + id + " not found."), HttpStatus.NOT_FOUND);
		}
		superheroService.deleteSuperheroById(id);
		return new ResponseEntity<Superhero>(HttpStatus.OK);
	}
		
}
